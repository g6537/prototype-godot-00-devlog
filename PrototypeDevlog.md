# 11/06/2022

Well, it's a bit late to be making this but let's get it started.

![image.png](./image.png)

Currently the new source of my headaches. It moves alright, depending upon which direction you press at first. The problem is that the box does not activate, or rather it actives and deactivates too fast. I handled something similar with Player_Char_2 by using an animation node, so maybe that's a good start? Play the 'Animation' until we stop moving? The timing for this is looking tricky. I could use delta perhaps somewhere?

Another thing to note is that it's very not intuitive. You have to press the F key, but you can't let go of it until it's done 'charging'. Hm.. I ought to look back at those timers from Player.cs

I have come back from looking at how the delta thing actually works. So now the plan is to re-work the EX-A method using delta, if it doesn't lose it's mind with _Process.

Update: It lost it's mind.

The actual pain of working with Godot is that some specific questions I have are not exactly in the Documentation. Either that or I am dumb.

# 19/06/2022
Standing here, I realize...
That I am a moron.

Execution doesn't truly 'work'. What I mean is that the override in public override void _Input() actually overrides some of the inputs, like if you try to move while charging the execution it overrides that and stops it. Hence me getting stuck on this nightmare. Jesus H. Christ I have never encountered such a massive headache when it comes to programming a mechanic.

Maybe I should go back to using a timer's wait time.

